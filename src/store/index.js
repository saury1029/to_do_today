/** @format */

import { writable } from 'svelte/store';
import shortid from 'shortid';
import dayjs from 'dayjs';

function createTodoStore() {
  let localTodos = JSON.parse(localStorage.getItem('__TODOS__')) || [];
  localTodos = localTodos.filter(
    (todo) => dayjs().diff(dayjs(todo.time), 'day') === 0,
  );
  const { set, update, subscribe } = writable(localTodos || []);

  return {
    subscribe,
    set,
    createTodo: (text) =>
      update((todos) => [
        {
          id: shortid.generate(),
          text,
          complete: false,
          time: dayjs().format('YYYY/MM/DD HH:mm:ss'),
        },
        ...todos,
      ]),
    deleteTodo: (id) =>
      update((todos) => todos.filter((todo) => todo.id !== id)),

    toggleTodo: (id) =>
      update((todos) =>
        todos.map((todo) =>
          todo.id === id
            ? {
                ...todo,
                complete: !todo.complete,
              }
            : todo,
        ),
      ),
  };
}

export const store = createTodoStore();
